#!/usr/bin/env python3
import hashlib
import os
import pathlib

class Splitter:

    def __init__(self, out_dir="./out/data/", split_size=1024 * 1024):
        if not os.path.isdir(out_dir):
            pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)

        if str(out_dir).endswith("/"):
            self.out_dir = str(out_dir)
        else:
            self.out_dir = str(out_dir) + "/"
        print(f"output dir: {out_dir}")

        self.split_size = split_size

    def split(self, filepath):
        try:
            with open(filepath, "rb") as in_file:
                result = []
                while True:
                    data = in_file.read(self.split_size)

                    if data == b"":
                        break

                    data_hash = hashlib.sha256(data).hexdigest()

                    result.append(data_hash)

                    out_file = open(self.out_dir + data_hash, "wb")
                    out_file.write(data)
                return result
        except:
            return []
