import os
import json


def get_structure(root_dir="./"):
    root_dir_formated = ""

    if root_dir.endswith("/"):
        root_dir_formated = root_dir[:-1]
    else:
        root_dir_formated = root_dir

    structure = {}

    for dirname, subdirList, fileList in os.walk(root_dir_formated):
        dir_levels = dirname.replace(root_dir, "", 1)

        structure[dir_levels] = {"files": [name for name in fileList]}

    return structure
