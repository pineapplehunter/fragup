#!/usr/bin/env python3
from splitter import Splitter
from walk import get_structure
from pathlib import Path
import json
import datetime

output_dir = (Path.home() / "backup").absolute()
src_dir = str(Path.home()) + "/Projects/"
def backup(args):
    splitter = Splitter(output_dir / "data", 1024 * 50)

    backup_folder_structure = get_structure(src_dir)

    output_structure = {}

    for key in backup_folder_structure.keys():
        for f in backup_folder_structure[key]["files"]:
            filepath = key + "/" + f
            print(filepath, end="")

            result = splitter.split(src_dir + filepath)
            print(f" -> {len(result)} hashes")

            if key not in output_structure:
                output_structure[key] = {"files":[]}
            output_structure[key]["files"].append({
                    "filename": f,
                    "hashs": result
                })
    json_structure = json.dumps(output_structure)
    (output_dir / "structures").mkdir(parents=True, exist_ok=True)

    with open(str(output_dir / "structures")+ "/" + str(datetime.datetime.now()).replace(" ", "_") + ".json", "w") as out:
        out.write(json_structure)   

if __name__ == "__main__":
    backup()
