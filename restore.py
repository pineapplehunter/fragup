from pathlib import Path
import json

home_dir = str(Path.home())
backup_dir = f"{home_dir}/backup"
restore_dir = f"{home_dir}/Projects"

def restore(args):
    structure = {}
    with open(f"{backup_dir}/structures/{args.snapshot}", "r") as snapshot:
        structure = json.loads(snapshot.read())
    
    print(f"stort restore [{len(structure.keys())}keys]")

    errors = []
    for key in structure.keys():
        for f in structure[key]["files"]:
            Path(f"{restore_dir}/{key}").mkdir(parents=True, exist_ok=True)
            try:
                with open(f"{restore_dir}/{key}/{f['filename']}","wb") as out:
                    print(f"restoring {f['filename']}")
                    for h in f["hashs"]:
                        with open(f"{backup_dir}/data/{h}","rb") as b:
                            out.write(b.read())
            except:
                errors.append(f"could not restore {f['filename']}")
