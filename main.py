from backup import backup
from restore import restore
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="an backup app")

    subparsers = parser.add_subparsers()

    parser_backup = subparsers.add_parser("backup")
    parser_backup.set_defaults(handler=backup)

    parser_restore = subparsers.add_parser("restore")
    parser_restore.add_argument("-s", "--snapshot-file", dest="snapshot", required=True)
    parser_restore.set_defaults(handler=restore)

    args = parser.parse_args()

    if hasattr(args, 'handler'):
        args.handler(args)
    else:
        parser.print_help()
